package com.example.Ejemplo01_3525;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ejemplo013525Application {

	public static void main(String[] args) {
		SpringApplication.run(Ejemplo013525Application.class, args);
	}

}
