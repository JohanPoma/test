package com.example.Ejemplo01_3525;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class Controlador {
    
    //http://localhost
    //http://localhost/
    @GetMapping("/s")
    public String principal()
    {
        return "index"; //index.html
    }
    
    //http://localhost/app
    @GetMapping("/app")
    public String Aplicaciones()
    {
        return "aplicaciones"; //aplicaciones.html
    }
    
    //http://localhost/music
    @GetMapping("/music")
    public String Musica()
    {
        return "musica"; //musica.html
    }
    
    //http://localhost/movie
    @GetMapping("/movie")
    public String Pelicula()
    {
        return "peliculas"; //peliculas.html
    }
}
